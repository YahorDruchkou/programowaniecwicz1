#Zadanie 1
# a - jest to student_1, b - jest to student_2; trzeba w terminalie wpisac naprzyklad a=25, b=23
from nbformat import read


a = int(input("Wpisz_wiek_studenta_1:"))
b = int(input("Wpisz_wiek_studenta_2:"))

if(a > b): print(f'student_1 jest starszy i ma {a} lat')
else: print(f'student_2 jest starszy i ma {b} lat')

with open("wiek1.txt.TXT", 'a') as file:
    file.write("student_1 jest starszy i ma 25 lat")


#Zadanie 2
a = int(input("Wpisz_wiek_studenta_1:"))

f = open('wiek2.txt', 'r')
f.read()

b = 23

if(a > b): print(f'student_1 jest starszy i ma {a} lat')
else: print(f'student_2 jest starszy i ma {b} lat')

with open("wiek2.TXT", 'a') as file:
    file.write("student_1 jest starszy i ma 25 lat")